#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Aplicaciones distribuidas en Internet
Curso: 2019 / 2020

David Carneros Prado
Sergio Gonzalez Velazquez
Jose Luís Mira Serrano

"""

import cmd
import os
from colorama import Fore, Style, init
import texttable as tt


class Shell(cmd.Cmd):
    '''Intérprete de comandos'''
    intro = Style.BRIGHT + "\n" + ("="*80 + "\n") + "\tIntroducción a los Blockchains\n"\
        + ("="*80 + "\n")\
        + ("\tAsignatura: Aplicaciones Distribuidas en Internet")\
        + ("\n\n\tDavid Carneros Prado\n")\
        + ("\tSergio González Velázquez\n")\
        + ("\tJosé Luis Mira Serrano\n")\
        + ("="*80 + "\n") + (Fore.YELLOW
                             + '\nIntroduzca help o ? para ver la lista de comandos disponibles.\n'
                             + Fore.RESET)

    prompt = Style.BRIGHT + Fore.RED + '<Dapp>' + Fore.RESET
    dapp = None
    account = None
    directory = './'
    init()

    @property
    def online(self):
        ''' Determina si se está conectado o no a una cuenta'''
        return self.dapp.active_account is not None

    def _compute_prompt_(self):
        '''Actualiza el prompt cuando se conecta/desconecta a una cuenta'''
        if self.online:
            self.prompt = Fore.GREEN + Style.BRIGHT + \
                '<Dapp(' + self.account + ')>' + Fore.RESET
        else:
            self.prompt = Fore.RED + Style.BRIGHT + '<Dapp>' + Fore.RESET

    def do_new_ballot(self, line):
        '''Permite a un usuario crear votaciones. 

        Uso: new_ballot "description" candidato1 candidato2 ... candidatoN

        Importante: definir descripción entre comillas dobles y separar 
        candidatos por espacios
        '''
        if not self.online:
            print("ERROR. You are not logged in")
            return

        #Quitar posibles espacios del principio/final de la línea
        line = line.strip()

        if not line[0] == '"':
            print('Error. Usage: new_ballot "description" candidato1 candidato2 ... candidatoN')
            return

        line = line[1:]
        end_description = line.find('"')

        if end_description < 0:
           print('Error. Usage: new_ballot "description" candidato1 candidato2 ... candidatoN')
           return
        
        description = line[0:end_description]
        line = line[end_description:]

        #Split por espacios para coger candidatos
        line = line.split(' ')
        candidates = []
        if len(line) > 1:
            candidates = line[1:]
        self.dapp.new_ballot(description, candidates)

    def do_add_candidate(self, line):
        """
        Permite al propietario de una votación añadir candidatos 
        a la misma.

        Uso: add_candidate <votacion> <candidato>

        donde <votacion> es el número de la votación, que puede consultarse
        con el comando "view_ballots"
        """

        if not self.online:
            print("ERROR. You are not logged in")
            return

        line = line.split(" ")
        if len(line) != 2 or not line[0].isdigit():
            print("Error. Usage: add_candidate <voting> <candidate>")
            return

        voting = line[0]
        candidate = line[1]
        self.dapp.add_candidate(int(voting), candidate)

    def do_close_candidates(self, line):
        """
        Permite al propietario de una votación cerrar la lista de candidatos

        Uso: close_candidates <votacion> 

        donde <votacion> es el número de la votación, que puede consultarse
        con el comando "view_ballots"
        """

        if not self.online:
            print("ERROR. You are not logged in")
            return

        line = line.split(" ")
        if len(line) != 1 or not line[0].isdigit():
            print("Error. Usage: close_candidates <voting>")
            return

        voting = line[0]
        self.dapp.close_candidates(int(voting))

    def do_vote(self, line):
        """
        Permite votar a algún candidato de la votación

        Uso: vote <votacion> <candidato>

        donde <votacion> es el número de la votación, que puede consultarse
        con el comando "view_ballots"
        """

        if not self.online:
            print("ERROR. You are not logged in")
            return

        line = line.split(" ")
        if len(line) != 2 or not line[0].isdigit():
            print("Error. Usage: vote <voting> <candidate>")
            return

        voting = line[0]
        candidate = line[1]
        self.dapp.vote(int(voting), candidate)

    def do_close_ballot(self, line):
        """
        Permite al propietario de una votación cerrar una votación

        Uso: close_ballot <votacion> 

        donde <votacion> es el número de la votación, que puede consultarse
        con el comando "view_ballots"
        """

        if not self.online:
            print("ERROR. You are not logged in")
            return

        line = line.split(" ")
        if len(line) != 1 or not line[0].isdigit():
            print("Error. Usage: close_ballot <voting>")
            return

        voting = line[0]
        self.dapp.close_ballot(int(voting))

    def do_description(self, line):
        """
        Permite consultar la descripción de una votación

        Uso: description <votacion> 

        donde <votacion> es el número de la votación, que puede consultarse
        con el comando "view_ballots"
        """
        line = line.split(" ")
        if len(line) != 1 or not line[0].isdigit():
            print("Error. Usage: description <voting>")
            return

        voting = line[0]
        self.dapp.description(int(voting))

    def do_status(self, line):
        """
        Permite consultar el estado de una votación

        Uso: status <votacion> 

        donde <votacion> es el número de la votación, que puede consultarse
        con el comando "view_ballots"
        """
        line = line.split(" ")
        if len(line) != 1 or not line[0].isdigit():
            print("Error. Usage: status <voting>")
            return

        voting = line[0]
        self.dapp.status(int(voting))

    def do_candidates(self, line):
        """
        Permite consultar el listado de candidatos de una votaciónn

        Uso: candidates <votacion> 

        donde <votacion> es el número de la votación, que puede consultarse
        con el comando "view_ballots"
        """
        line = line.split(" ")
        if len(line) != 1 or not line[0].isdigit():
            print("Error. Usage: candidates <voting>")
            return

        voting = line[0]
        self.dapp.candidates(int(voting))

    def do_winner(self, line):
        """
        Permite consultar el resultado de votaciones ya cerradas

        Uso: winner <votacion> 

        donde <votacion> es el número de la votación, que puede consultarse
        con el comando "view_ballots"
        """

        if not self.online:
            print("ERROR. You are not logged in")
            return

        line = line.split(" ")
        if len(line) != 1 or not line[0].isdigit():
            print("Error. Usage: winner <voting>")
            return

        voting = line[0]
        self.dapp.winner(int(voting))

    def do_login(self, line):
        '''
        Permite introducir la dirección del usuario que va a utilizar la Dapp.

        Uso: login account 1 | account 2 | ... | account 7 
        '''
        if self.online:
            print("ERROR. Ya está conectado.")
            return

        params = line.split(" ")

        if len(params) != 2 or params[0] != "account" or not params[1].isdigit():
            print("ERROR. Usage: login account <int>")
            return

        response = self.dapp.login(int(params[1]))

        if not response:
            print("ERROR. Cannot connect to " + line)
        else:
            print("Connected to " + response)
            self.account = line.replace(' ', '')

        self._compute_prompt_()

    def do_balance(self, line):
        """
        Devuelve el balance de la cuenta activa
        """
        if not self.online:
            print("ERROR. You are not logged in")
            return

        print(self.dapp.get_account_balance())

    def do_accounts_summary(self, line):
        """
        Devuelve las cuentas disponibles junto con 
        su balance.
        """

        headings = ['Account', 'Address', 'Balance (Ether)']
        account = []
        address = []
        balance = []

        info = self.dapp.get_accounts_summary()
        for i in range(len(info)):
            account.append(i)
            address.append(info[i][0])
            balance.append(info[i][1])

        print_table(headings, zip(account, address, balance))

    def do_view_ballots(self, line):
        """
        Devuelve la dirección de las votaciones junto con
        su propietario, descripción, estado y listado de 
        candidatos
        """

        headings = ['Voting', 'Address', 'Owner',
                    "Description", "Candidates", "Status"]
        voting = []
        address = []
        owner = []
        description = []
        candidates = []
        status = []

        info = self.dapp.get_votings()

        for i in range(len(info)):
            voting.append(i)
            address.append(info[i].get_address())
            owner.append(info[i].get_owner())
            candidates.append(info[i].get_candidates())
            description.append(info[i].get_description())
            status.append(info[i].get_status())

        print_table(headings, zip(voting, address, owner,
                                  description, candidates, status))

    def do_logout(self, line):
        '''Borra la dirección del usuario que estaba utilizando la Dapp '''
        if not self.online:
            print("ERROR. You are not logged in")
            return
        self.dapp.logout()
        self.account = None
        self._compute_prompt_()

    def do_clear(self, line):
        '''Comando que limpia nuestra terminal dejándola como recién abierta'''
        if os.name == "posix":
            os.system("clear")
        elif os.name == ("ce", "nt", "dos"):
            os.system("cls")

    def do_exit(self, line):
        '''Salir del intérprete'''
        print("\n" + "-"*80)

        return True

    def do_EOF(self, line):
        '''end of file '''
        return self.onecmd("exit")

    def emptyline(self):
        '''No realiza ninguna accion'''

    def default(self, line):
        '''
        El método default() es llamado cuando un comando introducido
        no es reconocido por el intérprete.
        '''
        print("Error. Comando no reconocido:", line)


def print_table(headings, rows):
    '''Pinta en consola una tabla con la cabecera y filas
    recibidas por agumentos.

    Hace uso de la libreria texttable

    '''
    tab = tt.Texttable()
    tab.header(headings)

    for row in rows:
        tab.add_row(row)

    table = tab.draw()
    print(table)
