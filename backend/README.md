# Compile contract 

Script para compilar el contrato y escribir el resultado en un json para que el frontend lo lea. 

## Funcionamiento: 🔧

```
python3 compile_contract.py <opciones>
```

Para mostrar la ayuda: opción -h.

Opciones:
- "-o" Indica el directorio de salida del resultado de la compilacion (por defecto: ../frontend/contract_data.json).
- "-n" Direccion del nodo que expone la api web3 (por defecto: http://localhost:22000).


# Test

## Funcionamiento: 🔧

```
python3 test.py <opciones>
```

Opciones:
- "-v" : Imprime mas informacion sobre los test que realiza

## Casos de prueba: 

Los test que se comprueban son: 
- Crear una votacion
- Añadir candidatos a la votacion. (Prueba que solo el propietario pueda añadir candidatos.)
- Añadir candiadtos repetidos. (prueba que no se pueden añadir candidatos repetidos)
- Cerrar la lista de candidatos. (Prueba que solo el propietario puede cerrar la lista)
- Votar. (Prueba que solo se puede votar una vez)
- Votar a candidatos que no existen. (Prueba que no se puede votar a candiatos inexistentes)
- Cerrar votacion (Prueba que solo el propietario puede cerrar la votacion)
- Ganador (Prueba que el ganador de la votacion es el esperado)


