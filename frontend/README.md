# Aplicación front-end

>Aplicación shell escrita en python3 que sirve como frontend de nuesta Dapp. Utiliza _pyweb3_ para acceder a la red del blockchain e interactuar con la Dapp, ofreciendo toda la funcionalidad requerida.

## Librerias utilizadas

- [pyweb3](https://pypi.org/project/colorama/). Librería basada en el API Web3.js de Javascript que permite interactuar con Ethereum. 
- [cmd](https://docs.python.org/3/library/cmd.html). Utilizado para construir un íntérprete de comandos. 
- [colorama](https://pypi.org/project/colorama/). Permite cambiar el color y estilo del texto y fondo de la consola.
- [texttable](https://pypi.org/project/texttable/). Módulo que permitir imprimir listas como datos tabulares.

## Ejecución

La red de blockchain está configurada para que exponga los puertos necesarios de los nodos quorum, por lo que el front-end puede ejecutarse en el host. Para ello, basta con lanzar el siguiente comando:

```shell
python3 dapp.py
```

Antes de iniciarse, la aplicación desbloquea varias direcciones de usuarios que pueden utilizar la Dapp.

## Funcionalidad implementada

La aplicación, de tipo shell, cuenta con la ayuda necesaria para su uso. Introduciendo la orden _?_ o _help_ podemos ver el listado de comandos disponibles. Para conocer el funcionamiento o uso de un determinado comando, basta con introducir la instrucción _help_ seguida del nombre del comando a consultar.

<p align="center">
<img src="/frontend/screenshots/help.png" width="450" > 
</p>

### Gestión de cuentas

#### Conectarse/Desconectarse de una cuenta

Para empezar a interactuar con nuestra Dapp, debemos seleccionar alguna de las cuentas disponibles. Para ello, podemos utilizar el comando ```login```.

```shell
login account <numero_cuenta>
```

Al conectarse a una determinada cuenta, se imprime por consola la dirección asociada a la misma.

Para cambiar de una cuenta a otra, es necesario desconectarnos de la cuenta activa y conectar a la cuenta deseada. Para desconectar la cuenta activa, puede utilizarse el comando ```logout```. La siguiente imagen muestra un ejemplo de conexión/desconexión a una de las cuentas de la red.

<p align="center">
<img src="/frontend/screenshots/login.png" width="450" > 
</p>

#### Listado de cuentas disponibles

Es posible conocer el listado de cuentas disponibles y el balance en Ether de cada una de ellas con el comando ```accounts_summary```. La salida es una tabla en la que la primera columna representa el índice de cuenta, utilizado para conectarse a una determinada cuenta con el comando ```login```.

```shell
new_ballot account <numero_cuenta>
```

<p align="center">
<img src="/frontend/screenshots/accounts_summary.png" width="450" > 
</p>



Estando conectado a una determinada cuenta, es posible conocer su balance con el comando ```balance```.

### Crear y gestionar votaciones

#### Crear votación

Un usuario puede crear una votación con el comando ```new_ballot```. Con el mismo comando, puede definir una descripción y añadir candidatos a la votación. Cuando se crea una votación, se retorna la dirección de la nueva votación.

```shell
new_ballot "descripción" candidato1 candidato2 ... candidatoN
```

Es importante definir la descripción entre comillas dobles y separar candidatos por espacios.

<p align="center">
<img src="/frontend/screenshots/new_ballot.png" width="450" > 
</p>

#### Información de una votación
El comando ```view_ballots``` imprime en forma tabular el listado de votaciones que se han creado, junto con su dirección, la dirección de su propietario, su estado, descripción y listado de candidatos.

<p align="center">
<img src="/frontend/screenshots/view_ballots.png" width="450" > 
</p>

Por otro lado, si lo que quiere conocerse es la descripción, estado o candidatos de una votación concreta, pueden utilizarse los siguientes comandos:

```shell
description <votacion>
```

```shell
candidates <votacion>
```

```shell
status <votacion>
```

Donde ```<votación>``` es el número de la votación, que puede consultarse con el comando ```view_ballots```

#### Añadir candidatos y cerrar listado de candidatos

Un usuario puede añadir candidatos a las votaciones en estado _Creating_ y cerrar la lista de candidatos para dar comienzo a la votación. Los comandos usados para ello son:

```shell
add_candidate <votacion> <candidato>
```

```shell
close_candidates <votacion>
```

Donde ```<votación>``` es el número de la votación, que puede consultarse con el comando ```view_ballots```.

<p align="center">
<img src="/frontend/screenshots/candidates.png" width="300" > 
</p>

#### Cerrar votación
Únicamente el propietario de una votación puede cerrarla utilizando el comando ```close_ballot```. Esto provoca que el estado de la votación pase a ```Ended```.

```shell
close_ballot <votacion>
```

Donde ```<votación>``` es el número de la votación, que puede consultarse con el comando ```view_ballots```

### Votar y consultar resultados

La aplicación también permite votar a algún candidato de la votación y consultar el resultado de votaciones ya cerradas. Es importante tener en cuenta que cada usuario sólo puede votar una vez y no se puede votar a candidatos inexistentes. 

Para votar, se utiliza el comando:

```shell
vote <votacion> <candidato>
```

Y, para consultar el resultado de una votación cerrada:

```shell
winner <votacion>
```

Donde ```<votación>``` es el número de la votación, que puede consultarse con el comando ```view_ballots```.

En el ejemplo de la siguiente captura, el usuario de la cuenta 0, que es el propietario de la votación 0, vota al candidato _prueba3_. Después, el usuario de la cuenta 1 vota al mismo candidato. Finalmente, el propietario cierra la votación y comprueba el resultado.

<p align="center">
<img src="/frontend/screenshots/vote.png" width="400" > 
</p>
