#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Aplicaciones distribuidas en Internet
Curso: 2019 / 2020

David Carneros Prado
Sergio Gonzalez Velazquez
Jose Luís Mira Serrano

"""

import json
from web3 import Web3, HTTPProvider
from web3.middleware import geth_poa_middleware
from colorama import Fore
import dapp_shell
from voting import Voting

CONFIG = "frontend_config.json"
BALLOT_DATA = "contract_data.json"


class Dapp:
    """
    Clase que hace uso de pyWeb3 para acceder
    a la red de blockchain y interactúar con nuestra Dapp
    """

    def __init__(self):
        self.w3 = None
        self.ballot = None
        self.accounts = []
        self.active_account = None
        self.votings = []

    def configure_provider(self):
        """
        Configura un proveedor Web3
        """
        try:
            with open(CONFIG) as json_file:
                self.provider = json.load(json_file)['provider']
                self.w3 = Web3(HTTPProvider(self.provider))
                # inject the poa compatibility middleware to the innermost layer
                # https://github.com/ethereum/web3.py/issues/898
                self.w3.middleware_onion.inject(geth_poa_middleware, layer=0)

        except IOError:
            print('Error opening settings file')
            exit()

    def unlock_accounts(self):
        """Desbloquea las cuentas para poder utilizarlas
        """
        # Get accounts
        self.accounts = self.w3.eth.accounts

        # Desbloquea las cuentas
        print("Desbloqueando cuentas para poder usar la DApp ...")
        for account in self.accounts:
            self.w3.geth.personal.unlockAccount(account, "")
            print("  Cuenta {}".format(account) +
                  Fore.GREEN + " done!" + Fore.RESET)

    def get_account_balance(self):
        """
        Obtiene el balance de la cuenta activa
        """
        if self.active_account:
            balance = self.w3.eth.getBalance(self.active_account)
            # Convert balance from wei to Ether
            return self.w3.fromWei(balance, "ether")
        print("ERROR. No está conectado")

    def get_accounts_summary(self):
        """
        Devuelve la dirección de cuentas disponibles junto
        con su balance
        """
        info = []
        for account in self.accounts:
            balance = self.w3.eth.getBalance(account)
            balance = self.w3.fromWei(balance, "ether")
            info.append((account, balance))

        return info

    def get_votings(self):
        """
        Devuelve las votaciones del blockchain
        """
        return self.votings

    def new_ballot(self, description, candidates):
        """
        Permite a un usuario crear una nueva votación.
        Cuando se crea una votación, retorna la dirección
        de la nueva votación para que otros usuarios puedan
        acceder a ella.
        """
        if self.active_account:
            try:
                with open(BALLOT_DATA) as json_file:
                    data = json.load(json_file)[0]
                    ballot = self.w3.eth.contract(
                        bytecode=data['bytecode'], abi=data['abi'])

                    tx_hash = ballot.constructor(candidates, description).transact()
                    tx_receipt = self.w3.eth.waitForTransactionReceipt(tx_hash)

                    if tx_receipt and tx_receipt['status'] == 1:
                        print("Ballot created: " +
                              tx_receipt['contractAddress'])
                        print(Fore.YELLOW + 'gasUsed: ' +
                              str(tx_receipt['gasUsed']) + Fore.RESET)
                        voting = Voting(
                            tx_receipt['contractAddress'], self.active_account, self.provider)
                        self.votings.append(voting)

                    else:
                        print("Error. Could not close ballot")

            except IOError:
                print('Error opening file with smart contract data')
                exit()

            except ValueError:
                #print("Error. " + str(e))
                print("Error. You probably tried to set invalid candidates")

    def add_candidate(self, voting_num, candidate):
        """
        Permite al propietario de una votación añadir candidatos
        a la misma.
        """
        if not voting_num in range(len(self.votings)):
            print("Error. Not found voting " + str(voting_num))
        else:
            try:
                response = self.votings[voting_num].add_candidate(self.active_account, candidate)
                if response and response['status'] == 1:
                    print("Done! Added candidate " + candidate)
                    print(Fore.YELLOW + 'gasUsed: ' +
                          str(response['gasUsed']) + Fore.RESET)
                else:
                    print("Error. Could not add candidate")

            except ValueError:
                #print("Error. " + str(e))
                if self.active_account != self.votings[voting_num].owner:
                    print("Error. Only owner can add candidates")
                elif self.votings[voting_num].get_status() != "Creating":
                    print("Error. You can only add candidates before closing the list of candidates")
                else:
                    print("Error. You probably tried to set a repeated candidate")



    def close_candidates(self, voting_num):
        """
        Permite al propietario de una votación cerrra la lista de
        candidatos de la misma
        """
        if not voting_num in range(len(self.votings)):
            print("Error. Not found voting " + str(voting_num))

        elif self.active_account != self.votings[voting_num].owner:
            print("Error. Only owner can close candidates list")

        else:
            try:
                response = self.votings[voting_num].close_candidates(self.active_account)
                if response and response['status'] == 1:
                    print("Done! Candidates closed ")
                    print(Fore.YELLOW + 'gasUsed: ' +
                          str(response['gasUsed']) + Fore.RESET)
                else:
                    print("Error. Could not close candidates list")
            except ValueError:
                if self.active_account != self.votings[voting_num].owner:
                    print("Error. Only owner can close candidates list")
                elif self.votings[voting_num].get_status() != "Creating":
                    print("Error. List of candidates is closed")

    def vote(self, voting_num, candidate):
        """
        Permite votar a algún candidato de la votación
        """
        if not voting_num in range(len(self.votings)):
            print("Error. Not found voting " + str(voting_num))

        else:
            try:
                response = self.votings[voting_num].vote(
                    self.active_account, candidate)
                if response and response['status'] == 1:
                    print("Done! Voted to candidate " + candidate)
                    print(Fore.YELLOW + 'gasUsed: ' +
                          str(response['gasUsed']) + Fore.RESET)
                else:
                    print("Error. Could not vote")
            except ValueError:
                if self.votings[voting_num].get_status() != "Voting":
                    print("Error. You can not vote in this state")
                elif not candidate in self.votings[voting_num].get_candidates():
                    print("Error. This candidate does not exists")
                else:
                    print("Error. This account has probably already voted")


    def close_ballot(self, voting_num):
        """
        Permite al propietario de una votación cerrar la votación
        """
        if not voting_num in range(len(self.votings)):
            print("Error. Not found voting " + str(voting_num))

        elif self.active_account != self.votings[voting_num].owner:
            print("Error. Only owner can close ballot")

        else:
            try:
                response = self.votings[voting_num].close_voting(self.active_account)
                if response and response['status'] == 1:
                    print("Done! Ballot closed ")
                    print(Fore.YELLOW + 'gasUsed: ' +
                          str(response['gasUsed']) + Fore.RESET)
                else:
                    print("Error. Could not close ballot")
            except ValueError:
                if self.active_account != self.votings[voting_num].owner:
                    print("Error. Only owner can close ballot")
                elif self.votings[voting_num].get_status() != "Voting":
                    print("Error. Voting can not be closed in this state")

    def winner(self, voting_num):
        """
        Permite consultar el resultado de votaciones cerradas
        """
        if not voting_num in range(len(self.votings)):
            print("Error. Not found voting " + str(voting_num))

        else:
            try:
                if(self.votings[voting_num].is_ballot_closed() == True):
                    response = self.votings[voting_num].winner()
                    print("Winner: " + response)
                else:
                    print("Error. Ballot is not ended")
                    
            except ValueError as e:
                print("Error. " + str(e))

    def description(self, voting_num):
        """
        Permite consultar la descripción de una votación
        """
        if not voting_num in range(len(self.votings)):
            print("Error. Not found voting " + str(voting_num))

        else:
            try:
                response = self.votings[voting_num].get_description()
                print("Description: " + response)
            except ValueError as e:
                print("Error. " + str(e))


    def candidates(self, voting_num):
        """
        Permite consultar los candidatos de una votación
        """
        if not voting_num in range(len(self.votings)):
            print("Error. Not found voting " + str(voting_num))

        else:
            try:
                response = self.votings[voting_num].get_candidates()
                print("Candidates: ")
                print('\n'.join(response))

            except ValueError as e:
                print("Error. " + str(e))


    def status(self, voting_num):
        """
        Permite consultar el estado de una votación
        """
        if not voting_num in range(len(self.votings)):
            print("Error. Not found voting " + str(voting_num))

        else:
            try:
                response = self.votings[voting_num].get_status()
                print("Status: " + response)
            except ValueError as e:
                print("Error. " + str(e))

    def login(self, account):
        """
        Docstring for login
        """
        if account > len(self.accounts):
            return False

        self.w3.geth.personal.unlockAccount(self.accounts[account], "")
        self.w3.eth.defaultAccount = self.accounts[account]
        self.active_account = self.accounts[account]
        return self.active_account

    def logout(self):
        """
        Docstring
        """
        self.active_account = None

    def run(self):
        """
        Docstring
        """
        self.configure_provider()

        if not self.w3.isConnected():
            print("Could not connect to Ethereum net")
            exit()

        self.unlock_accounts()

        shell = dapp_shell.Shell()
        shell.dapp = self
        shell.cmdloop()


if __name__ == "__main__":
    Dapp().run()
