import sys
import json
import argparse
from web3 import Web3
from colorama import Fore
from solcx import compile_files


def print_done():
    ''' 
    Imprime "done!" en verde para mostrar que algo 
    se ha reliazdo de forma correcta.
    ''' 
    print(Fore.GREEN + " done!" + Fore.RESET)


def print_error(error, line_break=0):
    '''
    Imprime mensaje de error
    '''
    print("\n"*line_break, end="")
    print(Fore.RED + "Error: " + Fore.RESET + error)


def compile_contract(network_ip, output):
    '''
    Comprueba que la red este activa. Compila el contrato y escribe el resultado
    '''
     # Conectar a la consola de web3 de la red para comprobar que la red esta funcionando
    print("Conectando a la red quorum con direccion {} ...".format(network_ip), end="")
    w3 = Web3(Web3.HTTPProvider(network_ip))
    if not w3.isConnected():
        print_error("No se puede conectar con el nodo", 1)
        sys.exit(1)

    print_done()

    print("Compilando el contrato... ", end="")

    try:
        # Compilando el contrato
        contracts = compile_files(["contract/Contract.sol"])
        # Separar el contrato principal de sus modulos
        main_contract = contracts.pop("contract/Contract.sol:VotingSystem")
    except Exception:
        print_error("en la compilacion de contrato", 1)
        sys.exit(1)

    print_done()

    print("Escribiendo fichero de salida en la ruta: '{}{}{}' ...".format(
        Fore.YELLOW, output, Fore.RESET), end="")
    try:
        with open(output, 'w') as outfile:
            data = {
                "abi": main_contract['abi'],
                "bytecode": main_contract['bin'],
                "accounts": w3.eth.accounts
            },
            json.dump(data, outfile, indent=4, sort_keys=True)
    except Exception:
        print_error("al escribir el resultado de la compilación", 1)
        sys.exit(0)

    print_done()


if __name__ == "__main__":

    PARSER = argparse.ArgumentParser(
        description="Script para la compilacion del contrato de la DApp")
    PARSER.add_argument("-o", type=str, help="Directorio de salida del resultado de la compilacion y despliegue",
                        default="../frontend/contract_data.json")
    PARSER.add_argument(
        "-n", type=str, help="Direccion de la consola web3 de la red (por defecto http://localhost:22000", default="http://localhost:22000")

    ARGS = PARSER.parse_args()

    NETWORK_IP = ARGS.n
    OUTPUT = ARGS.o

    compile_contract(NETWORK_IP, OUTPUT)
