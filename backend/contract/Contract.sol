pragma solidity ^0.6.1;
pragma experimental ABIEncoderV2;

/// @title A contract for implement a voting system
/// @notice This contract is part of a work developed for the subject Aplicaciones Distribuidas en Internet

contract VotingSystem {

    //These are the states in which the voting can be
    enum State {Creating, Voting, Ended}

    //Mapping the name of the vote to know how many votes you have
    mapping(string => uint256) private votes;

    //Mapping the id of a candidate to know his name
    mapping(uint256 => string) private candidate;

    //Description of the ballot
    string description;

    //List of candidates included in the ballot
    string[] private candidates;

    //Number of candidates in the ballot
    uint256 private candidatesCount = 0;

    //Mapping an account to see if you have voted
    mapping(address => bool) private voteEmitted;

    //Total votes of the ballot
    uint256 public totalVotes = 0;

    //Creator of the ballot
    address private ballotOwner;

    //State in which the vote is
    State private state;

    //Modifier to determine that only the creator of the ballot can modify it
    modifier onlyOwner() {
        require(msg.sender == ballotOwner, "Only owner can modify ballot");
        _;
    }

    ///@notice Modifier to indicate that a function can only be executed in a specific state of voting
    ///@param _state state to check
    modifier inState(State _state) {
        string memory stringState;
        if (_state == State.Creating) {
            stringState = "Creating";
        } else if (_state == State.Voting) {
            stringState = "Voting";
        } else {
            stringState = "Ended";
        }

        require(
            _state == state,
            strConcat("This function is only available in state", stringState)
        );
        _;
    }

    ///@notice Modifier to check if an account has already voted
    ///@param _voter addres to check
    modifier ifNotVoted(address _voter) {
        require(!voteEmitted[_voter], "Voter already vote");
        _;
    }

    ///@notice Function that returns a Boolean type depending on whether the specified candidate is in the voting candidate list
    ///@param _candidate name of the candidate to find in the list of candidates
    ///@return True or False depending if the candidate is in the list
    function findCandidateInCandidates(string memory _candidate)
        public
        view
        returns (bool)
    {
        bool finded = false;
        for (uint256 i = 0; i < candidatesCount; i++) {
            if (
                keccak256(bytes(candidates[i])) == keccak256(bytes(_candidate))
            ) {
                finded = true;
                break;
            }
        }

        return finded;
    }

    ///@notice Modifier to check if candidate is valid or not
    ///@param _candidate name to candidate to check if exists in candidates list
    modifier validCandidate(string memory _candidate) {
        bool finded = findCandidateInCandidates(_candidate);
        require(finded == true, "Candidate is not valid");
        _;
    }

    ///@notice Modifier to verify that the candidate does not exist yet
    ///@param _candidate Name to check if not exist
    modifier notExistentCandidate(string memory _candidate) {
        bool finded = findCandidateInCandidates(_candidate);
        require(finded == false, "Candidate already exists");
        _;
    }

    ///@notice Constructor
    ///@param _candidates List of candidates to introduce in the ballot
    ///@param _description string to describe the ballot
    constructor(string[] memory _candidates, string memory _description)
        public
    {
        ballotOwner = msg.sender;
        state = State.Creating;
        description = _description;
        for (uint256 i = 0; i < _candidates.length; i++) {
            addCandidate(_candidates[i]);
        }
    }

    ///@notice Function to add candidates to ballot
    ///@param _candidate Candidate to add to candidates list
    ///@dev This function only can be called if the state of the ballot is Creating, the candidate inserted does not exist and is the owner of the ballot who invokes it
    function addCandidate(string memory _candidate)
        public
        inState(State.Creating)
        notExistentCandidate(_candidate)
        onlyOwner
    {
        candidates.push(_candidate);
        candidatesCount++;
    }

    ///@notice Function to get the state of the ballot
    ///@return The state of the ballot
    function getVotingState() public view returns (string memory) {
        if (state == State.Creating) return "Creating";
        if (state == State.Voting) return "Voting";
        if (state == State.Ended) return "Ended";
    }

    ///@notice Function to get the description of the ballot
    ///@return The description of the ballot
    function getDescription() public view returns (string memory) {
        return description;
    }

    ///@notice Function to get the list of candidates of the ballot
    ///@return The list of the candidates of the ballot
    function getCandidates() public view returns (string[] memory) {
        string[] memory cands = new string[](candidatesCount);
        for (uint256 i = 0; i < candidatesCount; i++) {
            cands[i] = candidates[i];
        }
        return cands;
    }

    ///@notice Function to vote
    ///@param _voter Address of the voter
    ///@param _candidate Name of the candidate to vote
    ///@dev This function only can be called if the account who calls it has not voted, the state of the ballot is Voting and the candidate name specified exists in the candidates list
    function vote(address _voter, string memory _candidate)
        public
        ifNotVoted(_voter)
        inState(State.Voting)
        validCandidate(_candidate)
    {
        for (uint256 i = 0; i < candidatesCount; i++) {
            if (
                keccak256(bytes(candidates[i])) == keccak256(bytes(_candidate))
            ) {
                votes[candidates[i]]++;
                voteEmitted[_voter] = true;
            }
        }
    }

    ///@notice Function to get if ballot is closed or not
    ///@return True or False if ballot is closed or not
    function isBallotClosed() public view returns(bool){
        bool ballotClosed = false;

        if(state==State.Ended){
            ballotClosed = true;
        }
        return ballotClosed;
    }

    ///@notice Funtion to get the winner
    ///@return Name of the candidate who wins the ballot
    function getWinner() public view returns (string memory) {
        string memory winner = "";
        uint256 _votes = 0;

        for (uint256 i = 0; i < candidatesCount; i++) {
            if (votes[candidates[i]] > _votes) {
                _votes = votes[candidates[i]];
                winner = candidates[i];
            } else if(votes[candidates[i]] == _votes){
                winner = strConcat(winner," ");
                winner = strConcat(winner,candidates[i]);
            }
        }

        return winner;
    }

    ///@notice Function to close the candidates list
    function closeCandidatesList() public inState(State.Creating) onlyOwner {
        state = State.Voting;
    }

    ///@notice Function to close the ballot
    function closeBallot() public inState(State.Voting) onlyOwner {
        state = State.Ended;
    }

    ///@notice Auxiliar function to cancatenate two string
    ///@return Merged string of the two imputs
    function strConcat(string memory _a, string memory _b)
        internal
        pure
        returns (string memory)
    {
        bytes memory _ba = bytes(_a);
        bytes memory _bb = bytes(_b);

        string memory ab = new string(_ba.length + _bb.length);
        bytes memory ba = bytes(ab);
        uint256 k = 0;
        for (uint256 i = 0; i < _ba.length; i++) ba[k++] = _ba[i];
        for (uint256 i = 0; i < _bb.length; i++) ba[k++] = _bb[i];

        return string(ba);
    }
}
