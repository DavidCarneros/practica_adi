#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Aplicaciones distribuidas en Internet
Curso: 2019 / 2020

David Carneros Prado
Sergio Gonzalez Velazquez
Jose Luís Mira Serrano

"""

import unittest
from web3 import Web3, HTTPProvider
from web3.middleware import geth_poa_middleware
import json

PROVIDER = "http://localhost:22000"
BALLOT_DATA = "../../frontend/contract_data.json"
OWNER_INDEX = 0
USER_INDEX = 1
INITIAL_CANDIDATES = ["yellow", "red", "blue"]


def new_voting(provider, candidates, description, owner):
    """
    Crear una nueva votación.
    """
    try:
        with open(BALLOT_DATA) as json_file:
            data = json.load(json_file)[0]
            ballot = provider.eth.contract(
                bytecode=data['bytecode'], abi=data['abi'])
            tx_hash = ballot.constructor(
                candidates, description).transact({'from': owner})
            tx_receipt = provider.eth.waitForTransactionReceipt(tx_hash)

            voting = provider.eth.contract(
                address=tx_receipt['contractAddress'], abi=data['abi'])

            return voting

    except IOError:
        print('Error opening file with smart contract data')
        return False


def add_candidate(provider, user, voting, candidate):
    """
    Un usuario añade un candidato a la votación
    """
    tx_hash = voting.functions.addCandidate(candidate).transact({'from': user})
    tx_receipt = provider.eth.waitForTransactionReceipt(tx_hash)

    return tx_receipt


def close_candidates(provider, user, voting):
    """
    Un usuario cierra la lista de votaciones
    """
    tx_hash = voting.functions.closeCandidatesList().transact({'from': user})
    tx_receipt = provider.eth.waitForTransactionReceipt(tx_hash)

    return tx_receipt


def close_voting(provider, user, voting):
    """
    Un usuario cierra la lista de votaciones
    """
    tx_hash = voting.functions.closeBallot().transact({'from': user})
    tx_receipt = provider.eth.waitForTransactionReceipt(tx_hash)

    return tx_receipt


def vote(provider, voter, voting, candidate):
    """
    Un usuario vota a un candidato
    """
    tx_hash = voting.functions.vote(voter, candidate).transact({'from': voter})
    tx_receipt = provider.eth.waitForTransactionReceipt(tx_hash)

    return tx_receipt


def winner(voting):
    return voting.functions.getWinner().call()

class SmartContractTest(unittest.TestCase):
    """
    Batería de pruebas para garantizar el correcto
    funcionamiento del contranto
    """
    w3 = None
    owner_account = None
    user_account = None
    voting = None

    def test_0_add_candidates(self):
        """
        Prueba que sólo el propietario de la votación pueda
        añadir candidatos
        """
        # El propietario intenta añadir un candidato
        response = add_candidate(w3, owner_account, voting, "black")
        self.assertEqual(response['status'], 1)

        # Otro usuario intenta añadir un candidato
        self.assertRaises(ValueError, add_candidate, w3,
                          user_account, voting, "black2")

    def test_1_repeated_candidates(self):
        """
        Prueba que no se puede añadir a un candidatos varias veces
        """
        # El propietario intenta añadir un que no ha sido añadido
        response = add_candidate(w3, owner_account, voting, "pink")
        self.assertEqual(response['status'], 1)

        # El propietario intenta volver a añadir el mismo candidato
        self.assertRaises(ValueError, add_candidate, w3,
                          user_account, voting, "pink")

    def test_2_close_candidates(self):
        """
        Prueba que sólo el propietario de la votación pueda
        cerrar la lista de candidatos
        """

        # Otro usuario intenta cerrar la lista de candidatos
        self.assertRaises(ValueError, close_candidates, w3,
                          user_account, voting)

        # El propietario intenta cerrar la lista de candidatos
        response = close_candidates(w3, owner_account, voting)
        self.assertEqual(response['status'], 1)

    def test_3_vote(self):
        """
        Prueba que un usuario sólo puede votar una vez
        """
        # Un usuario intenta votar a un candidato
        response = vote(w3, user_account, voting, INITIAL_CANDIDATES[0])
        self.assertEqual(response['status'], 1)

        # El mismo usuario intenta volver a votar
        self.assertRaises(ValueError, vote, w3, user_account,
                          voting, INITIAL_CANDIDATES[0])

    def test_4_invalid_candidates(self):
        """
        Prueba que no se puede votar a candidatos inexistentes
        """
        # El propietario de la votación, que aún no ha votado,
        # intenta votar a un candidato inexistente
        self.assertRaises(ValueError, vote, w3,
                          user_account, voting, "noValid")

    def test_5_close_voting(self):
        """
        Prueba que sólo el propietario de la votación pueda
        cerrar la votación
        """
        # Otro usuario intenta cerrar la votación
        self.assertRaises(ValueError, close_voting, w3,
                          user_account, voting)

        # El propietario intenta cerrar la votación
        response = close_voting(w3, owner_account, voting)
        self.assertEqual(response['status'], 1)

    def test_6_winner(self):
        """
        Prueba que el ganador es el esperado
        """
        # El propietario consulta el ganador de la votación
        # El ganador debe ser el candidato votado en el tercer test
        response = winner(voting)
        self.assertEqual(response, INITIAL_CANDIDATES[0])


if __name__ == '__main__':
    w3 = Web3(HTTPProvider(PROVIDER))
    w3.middleware_onion.inject(geth_poa_middleware, layer=0)
    owner_account = w3.eth.accounts[OWNER_INDEX]
    user_account = w3.eth.accounts[USER_INDEX]

    if not w3.isConnected():
        print("Could not connect to Ethereum net")
        exit()

    # Desbloquea cuenta para el owner
    w3.geth.personal.unlockAccount(owner_account, "")

    # Desbloque cuenta para otro usuario
    w3.geth.personal.unlockAccount(user_account, "")

    # Crea una votación utilizada para el caso de prueba
    voting = new_voting(w3, INITIAL_CANDIDATES,
                        "Testing ballot", owner_account)

    if not voting:
        print("Could not created voting")
        exit()

    SmartContractTest.w3 = w3
    SmartContractTest.owner_account = owner_account
    SmartContractTest.user_account = user_account
    SmartContractTest.voting = voting

    unittest.main()
