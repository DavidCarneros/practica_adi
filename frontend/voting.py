#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Aplicaciones distribuidas en Internet
Curso: 2019 / 2020

David Carneros Prado
Sergio Gonzalez Velazquez
Jose Luís Mira Serrano

"""
import json
from web3 import Web3, HTTPProvider
from web3.middleware import geth_poa_middleware


BALLOT_DATA = "contract_data.json"


class Voting:
    """Encapsula el comportamiento y las operaciones de 
    una votación
    """

    def __init__(self, address, owner, provider):
        self.contract_address = address

        self.w3 = Web3(HTTPProvider(provider))
        self.w3.middleware_onion.inject(geth_poa_middleware, layer=0)

        with open(BALLOT_DATA) as json_file:
            data = json.load(json_file)[0]
            self.ballot = self.w3.eth.contract(
                address=address, abi=data['abi'])

        self.owner = owner

    def get_address(self):
        """Devuelve la dirección de la votación        
        """
        return self.contract_address

    def get_owner(self):
        """Devuelve la dirección del propietario
        """
        return self.owner

    def get_candidates(self):
        """Devuelve los candidatos de la votación
        """
        return self.ballot.functions.getCandidates().call()

    def get_status(self):
        """Devuelve los candidatos de la votación
        """
        return self.ballot.functions.getVotingState().call()

    def get_description(self):
        """Devuelve la descripción de la votación
        """
        return self.ballot.functions.getDescription().call()

    def winner(self):
        """Devuelve el candidato ganador 
        """
        return self.ballot.functions.getWinner().call()

    def is_ballot_closed(self):
        """Comprueba si la votación está cerrada
        """
        return self.ballot.functions.isBallotClosed().call()

    def add_candidate(self, user, candidate):
        """Añade un candidato a la votación
        """
        tx_hash = self.ballot.functions.addCandidate(
            candidate).transact({'from': user})
        tx_receipt = self.w3.eth.waitForTransactionReceipt(tx_hash)
        return tx_receipt

    def close_candidates(self, user):
        """Cierra la lista de candidatos
        """
        tx_hash = self.ballot.functions.closeCandidatesList().transact({
            'from': user})
        tx_receipt = self.w3.eth.waitForTransactionReceipt(tx_hash)
        return tx_receipt

    def vote(self, voter, candidate):
        """Vota a un candidato
        """
        tx_hash = self.ballot.functions.vote(
            voter, candidate).transact({'from': voter})
        tx_receipt = self.w3.eth.waitForTransactionReceipt(tx_hash)
        return tx_receipt

    def close_voting(self, user):
        """Cierra la votación
        """
        tx_hash = self.ballot.functions.closeBallot().transact({
            'from': user})
        tx_receipt = self.w3.eth.waitForTransactionReceipt(tx_hash)
        return tx_receipt
