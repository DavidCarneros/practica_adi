# Introducción a blockchains

Prácticas de la asignatura Aplicaciones distribuidas en internet. 

### Integrantes y roles: 

- Sergio González Velázquez   -->  Frontend y test
- David Carneros Prado        -->  Backend (Red quorum)
- José Luis Mira Serrano      -->  Backend (Contract)


### Requisitos necesarios:

#### Requisitos de equipo

Los requisitos necesarios para el correcto funcionamiento del equipo son: 
- Al menos 4GB de memoria ram
- Docker Engine 18.02.0+
- Docker Compose 1.21+

Nosotros lo hemos probado con equipos con las siguientes características: 
- Sistema operativo __Linux Mint 19.2__ / __Ubuntu 18__ / __MacOS mojave__ 
- Memoria: __16GB__
- __Docker 18.09.7__ 
- __Docker Compose 1.24.1__ 

#### Software necesario

Para el funcionamiento de la práctica es necesario tener instalado: 
- Python 3.6 
- Compilador de solidity 0.6.2 
- virtualenv (opcional pero recomendable)

##### Para instalar el compilador de solidity:
```shell
sudo add-apt-repository ppa:ethereum/ethereum
sudo apt-get update
sudo apt-get install solc
```

##### Librerías usadas

Es recomendable crear un entorno virtual para instalar los requerimientos de python para poder ejecutar la practica: 
```shell
virtualenv -p python3 quorum-env
source quorum-env/bin/activate
```

Y instalamos las dependencias: 
```shell
pip3 install -r requirements.txt 
```


Las librerías más importantes que usamos son: 
- colorama
- py-solc-x
- requests
- web3


### Como ejecutar la practica 

En la práctica hay distintos README para explicar cada parte. Desde esta sección vamos a mostrar como ejecutar la práctica y se va a referenciar a los diferentes readme.

##### Ejecutar la red: 

El primer paso para poder ejecutar la práctica es lanzar la red. Para ello nos posicionamos en el directorio `backend/quorum-network` y ejecutamos el el script para lanzar la red: 
```shell
python3 network.py -m start
```

Para más información de cómo funciona el script y las demás opciones que tiene (parar, scale-in, scale-out) ver el archivo: [README del script de la red](backend/quorum-network/README.md)

##### Compilar el contrato

Tras ejecutar la red, el siguiente paso es compilar el contrato. Para ello nos posicionamos en el directorio `backend` y ejecutamos el script para compilar el contrato (muy importante que este la red activa puesto que también comprueba que la red funciona)

```shell
python3 compile_contract.py
```

Esto genera un archivo json con el abi y el bytecode que sera usado en el frontend. [README del script compile_contract y tests](backend/README.md)

##### Ejecutar test (opcional)

Es aconsejable ejecutar los test para comprobar que el contrato funciona correctamente. Para ello nos posicionamos en el directorio `backend/test` y ejecutamos el script: 
```shell
python3 test.py -v
```
El argumento "-v" es para que se muestre información sobre los test. 

Los test que se comprueban son: 
- Crear una votación.
- Añadir candidatos a la votación (Prueba que solo el propietario pueda añadir candidatos).
- Añadir candidatos repetidos (prueba que no se pueden añadir candidatos repetidos).
- Cerrar la lista de candidatos (Prueba que solo el propietario puede cerrar la lista).
- Votar (Prueba que solo se puede votar una vez).
- Votar a candidatos que no existen (Prueba que no se puede votar a candidatos inexistentes).
- Cerrar votación (Prueba que solo el propietario puede cerrar la votación).
- Ganador (Prueba que el ganador de la votación es el esperado).

[README del script compile_contract y tests](backend/README.md)

##### Ejecutar el frontend 

Para ejecutar el frontend nos posicionamos en el directorio `fronted` y ejecutamos la aplicación: 

```shell
python3 dapp.py
```

Para saber el funcionamiento del frontend leer el readme del frontend. 

[README FRONTEND IMPORTANTE LEER](frontend/README.md)
