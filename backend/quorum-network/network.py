#!/usr/bin/python3

'''
Script para lanzar, parar y escalar la red quorum
'''

from enum import Enum
from time import sleep
import argparse
import subprocess
import json
from colorama import Fore
import requests

CONSENSUS = 'QUORUM_CONSENSUS=raft'
CLUSTER_CONFIG_DIR = '.'
NODES = {}
NODES_STATIC = {}
NODES_GENERATE = {}


class Modes(Enum):
    ''' 
    Enumerado con las funciones del script
    '''
    scale_in = 'scale-in'
    scale_out = 'scale-out'
    start = 'start'
    stop = 'stop'
    inspect = 'inspect'

    def __str__(self):
        return self.value


def scale_in(node):
    ''' 
    Añade el nodo al peer, obtiene su peerId y se enciende el nuevo nodo pasandole 
    este ID
    '''
    enode = NODES[node]

    data = {
        "jsonrpc": "2.0",
        "method": "raft_addPeer",
        "params": [enode],
        "id": 1
    }

    print("Adding new node to peer...", end=" ")
    r = requests.post("http://localhost:22000", json=data)
    sleep(2)
    print(Fore.GREEN + " done!" + Fore.RESET)

    raft_id = r.json()['result']
    command_info = subprocess.run(['echo "export RAFTID={}" > config/raftID'.format(
        raft_id)], stdout=subprocess.PIPE, shell=True, cwd=CLUSTER_CONFIG_DIR)
    command_info.check_returncode()

    launch_docker(node)

    print("Wait to connect node with the cluster... (Approx 30s)", end=" ")
    sleep(30)
    print(Fore.GREEN + "  done!" + Fore.RESET)
    print(node + Fore.GREEN + " launch!" + Fore.RESET)


def scale_out(node):
    '''  
    Obtiene el peer id del nodo que se quiere eliminar. 
    Despues elimina el nodo del peer y apaga el contenedor
    '''

    enode = NODES[node]

    data = {
        "jsonrpc": "2.0",
        "method": "raft_getRaftId",
        "params": [enode],
        "id": 1
    }
    print("Removing node to peer...", end=" ")
    # Obtener el peer id del nodo a borrar
    response = requests.post("http://localhost:22000", json=data)
    raft_id = response.json()["result"]
    data = {
        "jsonrpc": "2.0",
        "method": "raft_removePeer",
        "params": [raft_id],
        "id": 1
    }
    # Borrar ese nodo del peer
    response = requests.post("http://localhost:22000", json=data)
    print(Fore.GREEN + " done!" + Fore.RESET)

    print("Stoping and removing {}...".format(node), end=" ")

    all_nodes = {**NODES_GENERATE, **NODES_STATIC}

    for node_name in all_nodes[node]:
        command_info = subprocess.run([' docker-compose stop {}'.format(node_name)],
                                      stdout=subprocess.PIPE, shell=True, cwd=CLUSTER_CONFIG_DIR)
        command_info.check_returncode()

    print(Fore.GREEN + "Removed!" + Fore.RESET)


def start():
    ''' Encuende la red con 3 nodos iniciales [nodo1,nodo2,nodo3] '''
    print("Starting the quorum network")
    print("This may take a few minutes.")

    # Launch static nodes
    for node in [*NODES_STATIC]:
        launch_docker(node)

    print("Launching web interface")
    launch_docker("cakeshop")
    sleep(5)
    print("In a few moments the cluster will be operational...")

    sleep(20)
    print("The web interface will be operational at " + Fore.YELLOW +
          "http://localhost:8999 " + Fore.RESET + " (it's take 1 minute to start)")
    print(Fore.GREEN + "Quorum network started!" + Fore.RESET)


def stop():
    ''' Apaga todos los nodos '''
    print("Stoping containers")
    command_info = subprocess.run(
        [' docker-compose stop'], stdout=subprocess.PIPE, shell=True, cwd=CLUSTER_CONFIG_DIR)
    check_return_code(command_info, "docker-compose stop")
    print("Remove containers")
    command_info = subprocess.run(
        [' docker-compose rm -f'], stdout=subprocess.PIPE, shell=True, cwd=CLUSTER_CONFIG_DIR)
    check_return_code(command_info, "docker-compose rm -f")


def check_return_code(process, description):
    ''' Comprueba que el comando se ha ejecutado correctamente '''
    if(process.returncode is not 0):
        print(Fore.RED + "Error launching: " + description + Fore.RESET)
        stop()


def launch_docker(node_name):
    ''' Funcion que lanza el contenedor que se le pase como argumento  '''

    command_info = subprocess.run([CONSENSUS + ' docker-compose up -d ' + node_name],
                                  stdout=subprocess.PIPE, shell=True, cwd=CLUSTER_CONFIG_DIR)
    check_return_code(command_info, "docker-compose up -d " + node_name)


def get_node_status(node_name):
    ''' Salta excepción cuando el container no existe '''

    status = subprocess.run(["docker inspect --format='{{json .State}}'  quorum-network_" +
                             node_name + "_1"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)

    if status.returncode is not 0:
        return False
    else:
        return json.loads(status.stdout.decode())


def check_node_running(node_name):
    '''
    Comprueba que un nodo esta funcionando
    '''
    status = get_node_status(node_name)

    if not status:
        return False
    else:
        return status["Running"]


def print_node_status(status):
    ''' Imprime el estado del nodo '''

    print("Status: ", end='')
    if status["Status"] == "running":
        print(Fore.GREEN + status["Status"] + Fore.RESET)

    else:
        print(Fore.RED + status["Status"] + Fore.RESET)

    print()
    # Printing running, paused, restarting, dead values
    for stat in ["Running", "Paused", "Restarting", "Dead"]:
        print(stat + ":", end='')
        if status[stat]:
            print(Fore.GREEN + str(status[stat]) + Fore.RESET)

        else:
            print(Fore.RED + str(status[stat]) + Fore.RESET)

    # Printing pid value
    print("Pid: " + str(status["Pid"]))

    # Printing started at
    print("StartedAt: " + status["StartedAt"])

    # Priting finished At
    print("FinishedAt: " + status["FinishedAt"])


if __name__ == "__main__":

    PARSER = argparse.ArgumentParser(description='Escalado de quorum')
    PARSER.add_argument('-m', type=Modes, choices=list(Modes),
                        help="Modo [start|scale_in|scale_out|stop|inspect]", required=True)
    PARSER.add_argument(
        '-d', type=str, help="Directorio donde está la configuración de los nodos previamente preparados", default='.')
    PARSER.add_argument(
        '-n', type=str, help="Nombre del nodo ha lanzar/eliminar/consultar estado")
    PARSER.add_argument(
        '-c', type=str, help="Configuracion del script", default='config.json')

    ARGS = PARSER.parse_args()

    CLUSTER_CONFIG_DIR = ARGS.d

    with open(ARGS.c) as json_file:
        CONFIG = json.load(json_file)
        NODES = CONFIG["enode"]
        NODES_STATIC = CONFIG["staticNodes"]
        NODES_GENERATE = CONFIG["nodesToGenerate"]

    if ARGS.m == Modes.start:
        start()
    elif ARGS.m == Modes.stop:
        stop()

    elif ARGS.m == Modes.inspect:

        if ARGS.n not in NODES_GENERATE and ARGS.n not in NODES_STATIC:
            print(Fore.RED + "Error. " + ARGS.n +
                  " has not been configuFore.RED" + Fore.RESET)

        else:
            STATUS = get_node_status(ARGS.n)
            if(not STATUS):
                print(Fore.RED + "Error: No such node: " + ARGS.n + Fore.RESET)
            else:
                print_node_status(STATUS)

    elif ARGS.m == Modes.scale_in:
        # Nodo no preconfigurado
        if ARGS.n in NODES_STATIC:
            print(Fore.RED + "Error. Unable to scale an initial node" + Fore.RESET)

        elif ARGS.n not in NODES_GENERATE:
            print(Fore.RED + "Error. " + ARGS.n +
                  " has not been configuFore.RED" + Fore.RESET)
            print(
                "\nTo scale a not preconfigured node you must have followed this steps:")
            print("\t-Define it keys")
            print("\t-Add to permissions file")
            print("\t-Add to docker-compose")

        # Si el nodo ya está está funcionando
        elif check_node_running(ARGS.n):
            print(Fore.RED + "Error. " + ARGS.n +
                  " is already running" + Fore.RESET)

        else:
            scale_in(ARGS.n)

    elif ARGS.m == Modes.scale_out:
        # Nodo no está configurado y por tanto, no funcionando
        if (ARGS.n not in NODES_GENERATE) and (ARGS.n not in NODES_STATIC):
            print(Fore.RED + "Error. " + ARGS.n +
                  " is not configured" + Fore.RESET)

        # Nodo no está funcionando
        elif not check_node_running(ARGS.n):
            print(Fore.RED + "Error. " + ARGS.n +
                  " is not running" + Fore.RESET)

        else:
            scale_out(ARGS.n)
