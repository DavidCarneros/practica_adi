# Quorum network 

La definición de las maquinas esta cambiada para que en el peer inicial solo cuente con los nodos estáticos (node1, node2 y node3). Y en el permissions.json están los nodos permitidos para ser lanzados. Para simplificar los nodos están pre-definidos en el docker-compose, pero si se quiere añadir otro nodo tan solo es necesario definir las claves nuevas, añadirlo al archivo de permisos y añadir este nuevo nodo al docker-compose.

En un principio el peer inicial es de 3 (los nodos iniciales). Despues cuando el cluster escale añadirá los nuevos nodos al peer y se lanzará el nodo.

Tanto los archivos de configuración como el docker compose han sido modificados por nosotros, pero han sido sacados de: https://github.com/jpmorganchase/quorum-examples 


## Requisitos 📋
El funcionamiento del script y del clúster ha sido probado en un sistema con las siguientes características:

- Sistema operativo __Linux Mint 19.2__ / __Ubuntu 18__
- Memoria: __16GB__
- __Python 3.6__ y los paquetes definidos en el requirements.txt
- __Docker 18.09.7__ 
- __Docker Compose 1.24.1__ 

No obstante, de acuerdo a los requisitos definidos para el ejemplo de [7nodes](https://github.com/jpmorganchase/quorum-examples ), basta con:
- Al menos  4GB de memoria ram
- Docker Engine 18.02.0+
- Docker Compose 1.21+

## Funcionamiento: 🔧

```
python3 network.py <opciones>
```

Para mostrar la ayuda: opción -h.

Opciones:
- "-m" Modo de funcionamiento del scprit
-- "-m start" Inicia el clúster (los tres nodos estáticos)
-- "-m stop" Apaga el clúster y borra los contenedores
-- "-m scale-in" escala el clúster (requiere argumento -n)
-- "-m scale-out" elimina un nodo del cluster (requiere argumento -n)
-- "-m inspect" comprueba el estado de un nodo del cluster (requiere argumento -n)
- "-n" nombre del nodo que se va a añadir o eliminar [node4|node5|node6|node7]
- "-d" (opcional) Directorio donde esta la configuracion del cluster (valor por defecto "/cluster-config")
- "-c" (opcional) Archivo donde esta la configuracion del script (valor por defecto "config.json")
- "-h" Muestra la ayuda

## Ejemplos: 🚀

#### Inicar el cluster.

Se inicia la red con los 3 nodos estáticos (node1, node2 y node3). También se lanza un nodo "cakeshop" para poder ver el clúster a través de una web app. `http://localhost:8999` Tarda aproximadamente 1 minuto en estar todo operativo.
```
python3 network.py -m start
```

Tras inicio del cluster, el endpoint para conectar con web3 estará en `http://localhost:22000` (nodo1)

#### Añadir un nodo.
Se añade un nodo a la red. Para ello se manda una petición http para añadir el enode del nuevo nodo al clúster. Esta petición devuelve el raftID que se le asigna al nuevo nodo. Cuando este nodo arranque se le pasa este raftID a través del parámetro --joinexistingraft raftID
```
python3 network.py -m scale-in -n node4
```

#### Eliminar un nodo.
Se elimina un nodo del clúster. Para ello se mandan 2 peticiones http. La primera para pedir cual es el raftID dado un enode. Una vez que se tiene el raftID se manda una petición para eliminar ese nodo del raft y después se apaga el nodo.
```
python3 network.py -m scale-out -n node4
```

#### Comprobar el estado de un nodo.
Devuelve el estado, fecha de lanzamiento y fecha de finalización de un nodo. Para ello, se utiliza el comando `docker inspect`, que devuelve el estado de un determinado contenedor. Esto significa que si un nodo ha sido configurado pero aún no se ha lanzado, no se puede ver su estado.

También es posible ver el estado de los nodos a través de la interfaz gráfica disponible en `http://localhost:8999`. 
```
python3 network.py -m inspect -n node4
```

#### Parar el clúster. 
Se paran todos los nodos activos y se eliminan los contenedores
``` 
python3 network.py -m stop
```

## Cakeshop

Cakeshop es una herramienta desarrollada por [jpmorganchase](https://github.com/jpmorganchase/cakeshop) que ofrece una interfaz gráfica a través de aplicación web y que nosotros utilizamos para poder visualizar los nodos del clúster.

Hemos tenido que modificar la configuración original para que haga uno de las direcciones IP de los nodos y no utilice la sentencia `host.docker.internal`, ya que no es válido en sistemas Linux.

<img src="https://gyazo.com/caf512eae921db09e65473022869e0ee">

Tras escalar el cluster podemos ver como aumenta el peer y se conecta el nodo

<p>
<img src="https://gyazo.com/69d0dfe2fa98a6809749b6698483f51f" width="45%">
<img src="https://gyazo.com/4c4dd21eb49eaddd459b4336bba081be" width="45%">
</p>